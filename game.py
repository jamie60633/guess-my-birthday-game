from random import randint

name_input = input("Hi! What is your name? ")
yn_input = ''
cnt = 1
while yn_input not in ["yes","Y","y","Yes"]:
  m = randint(1,12)
  yyyy = randint(1950,2020)
  yn_input = input(f"Guess {cnt} : {name_input} were you born in {m} / {yyyy} ?\nyes or no? ")
  if yn_input in ["yes","Y","y","Yes"]:
    print("I knew it!")
    exit()
  else: #yn_input in ["no","N","n","No"]:
    if cnt < 5:
      print("Drat! Lemme try again!")
    else:
      print("I have other things to do. Good bye.")
      exit()
  cnt+=1